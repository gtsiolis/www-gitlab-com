---
layout: markdown_page
title: "Blueprints"
---

## **Business Operations**

## Who we are


**Teams**


*  Business Systems Analysts
Function: process and system improvements


*  Data Ops 
Function: Data Warehouse 

*  IT Help Desk
Function: Supports the Customer Portal: Licensing & Transactions

*  IT Operations 
Function: support the GitLab team to have the tools to do their work (laptops, tool access)





## What we're doing


*  [Business Operations board](https://gitlab.com/groups/gitlab-com/-/boards/1201212?label_name[]=BusinessOPS)
*  Access Request Board
*  Laptop Request Board

### How we do it
### Current Milestone
#### Distributed Ops Milestones
### Roadmaps (Product puts on sidebar -- sid likes)

## Resources 
### System Diagram
### Ownership-Table/Tech-Stack/Contract-Data
### Process References
### Strategic and Financial Planning Links
### User Journey Diagram
### Portal Responsibility Table

## Metrics
#### Definitions

## Procurement (point to existing documentation until procurement manager onboard)
#### 6060
#### etc

## Blueprints
#### Like this
