---
layout: markdown_page
title: "IAM.2.09 - Full Disk Encryption"
---
 
## On this page
{:.no_toc}
 
- TOC
{:toc}
 
# IAM.2.09 - Full Disk Encryption
 
## Control Statement

Where full disk encryption is used, logical access must be managed independently of operating system authentication; decryption keys must not be associated with user accounts.
 
## Context

Separating user accounts from decryption keys decreases the likelihood that an attacker with possession or control of a GitLab system can access any data contained on that system.
 
## Scope
This control applies to:
   * GitLab.com
   * System that support the operation of GitLab.com
   * All applications that store or process financial data

## Ownership
TBD
 
## Guidance
TBD
 
## Additional control information and project tracking
Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Full Disk Encryption issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/820) . 
 
### Policy Reference
TBD
 
## Framework Mapping
TBD